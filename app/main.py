__title__ = 'Berries'
__slogan__ = 'Ride this wave. Start the next!'
__slogan2__ = 'For the youth, by the youth.'

import logging
import threading
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.screenmanager import *
from kivy.core.window import Window
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.widget import Widget
from kivy.uix.textinput import TextInput
from kivy.uix.image import *
from kivy.uix.scrollview import *
from kivy.uix.carousel import Carousel
from kivy.metrics import dp
from kivy.utils import *
from kivy.clock import Clock
from kivy.animation import *
from kivy.atlas import Atlas
from kivy.storage.jsonstore import JsonStore
import webscript

trail = Atlas('assets/trail.atlas')

membank = JsonStore('membank.json')


def prepare_fake_mall():
    global fake_items, fake_items_2

    fake_items = [{
        'id': '262amjiia',
        'description': 'Blue Corporate Long-Sleeve Shirt',
        'price': '15749.99',
        'img_path': 'assets/ronaldo.jpg',
        'shipping_days': '3 days',
        'store': 'CR7 Stores'
    }, {
        'id': '262amjqqqa',
        'description': 'Italian Brown Leather Shoes',
        'price': '6299.99',
        'img_path': 'assets/shoes.png',
        'shipping_days': '3 days',
        'store': 'Vogue Fashion'
    }, {
        'id': '262amjqa',
        'description': 'Feather Slippers',
        'price': '5249.99',
        'img_path': 'assets/slippers.png',
        'shipping_days': '3 days',
        'store': 'Dope Lanky'
    }, {
        'id': '262amjqa',
        'description': 'Tie and Dye Shirt',
        'price': '5754.99',
        'img_path': 'assets/dieshirt.png',
        'shipping_days': '3 days',
        'store': 'Dope Lanky'
    }]

    fake_items_2 = [{
        'id': '262amjiia',
        'description': 'Dot Portrait Shirt',
        'price': '8399.99',
        'img_path': 'assets/tac_shirt.png',
        'shipping_days': '3 days',
        'store': 'TAC Clothing'
    }, {
        'id': '262amjqqqa',
        'description': 'FILA High-Tops',
        'price': '6299.99',
        'img_path': 'assets/fila_boots.png',
        'shipping_days': '3 days',
        'store': 'Vogue Fashion'
    }, {
        'id': '262amjqa',
        'description': 'Corporate Shredded Checkered Shirts',
        'price': '7349.90',
        'img_path': 'assets/shirt_2.png',
        'shipping_days': '3 days',
        'store': 'Dope Lanky'
    }]

    dict__ = {}

    for item in fake_items:
        dict__[item['id']] = item

    for item_2 in fake_items_2:
        dict__[item['id']] = item_2

    membank.put('mall_cache', mall_cache=dict__)


prepare_fake_mall()


try:
    from plyer import vibrator
except:
    pass


Window.size = (490, 700)
Window.clearcolor = (.75, .75, .75, 1)


global screen_history
screen_history = []


emblem_font = 'assets/emblem_font.ttf'
light = 'assets/regular.ttf'
bold = 'assets/bold.ttf'


def fontscale(int_):
    return int(dp(int_))

small_font = fontscale(10)
mid_font = fontscale(12)
big_font = fontscale(20)
bigger_font = fontscale(26)
biggest_font = fontscale(35)


emblem_color = (0, 0, 0, 2)
emblem_background_color = (.8, .1, .1, 1)


pink = get_color_from_hex('ff69b4')
dark_pink = get_color_from_hex('de4893')
green = '10c45d'
blue = get_color_from_hex('1087de')
tw_color = '11c1ff'
fb_color = '3a5ba2'


def format_price(str_):
    str_list = list(str_)
    str_list.reverse()

    for a in range(6, len(str_list), 3):
        str_list[a] += ','

    str_list.reverse()
    str_ = ''.join(str_list)
    return str_


def do_vibrate(secs):
    if platform == 'android':
        try:
            vibrator.vibrate(secs)
        except:
            logging.info('Unable to vibrate')


class Webview(object):
    pass


class Screen(Screen):
    def __init__(self, **kwargs):
        super(Screen, self).__init__(**kwargs)

    def on_enter(self):
        Window.clearcolor = (.75, .75, .75, 1)

        print('Entering %s' % self.name)

        global screen_history
        screen_history.append(self.name)

    def on_exit(self):
        print('Leaving screen %s' % self.name)


class TabsHolder(BoxLayout):
    def update(self, dict_):
        for kw in dict_:
            eval('self.%s = %s' % (kw, dict_[kw]))

    def select(self, title):
        index = -1

        for butt in self.theTitleBar.children:
            butt.background_color = self.inactive_color
            butt.font_name = light
            butt.color = (1, 1, 1, 1)

            if butt.text == title:
                butt.color = self.active_fg
                butt.background_color = self.active_color
                index = self.theTitleBar.children.index(butt)

                # butt.on_press()
                self.theCarousel.load_slide(self.theCarousel.slides[-1 - index])

                butt.on_release()

            index -= 1

        self.theTitleBarS.scroll_x = -(index) * self.head_size_x

    def __init__(self, **kwargs):
        super(TabsHolder, self).__init__(**kwargs)

        self.orientation = 'vertical'
        thisTH = self

        self.title_padding = [0, 5, 0, 0]
        if 'title_padding' in kwargs:
            self.title_padding = kwargs['title_padding']

        self.active_fg = (0, 0, 0, 1)
        if 'active_fg' in kwargs:
            self.active_fg = kwargs['active_fg']

        self.inactive_color = kwargs['inactive_color']
        self.active_color = kwargs['active_color']
        self.head_size_x = kwargs['head_sizeX']

        self.title_spacing = 1.5
        if 'title_spacing' in kwargs:
            self.title_spacing = kwargs['title_spacing']

        self.theTitleBar = GridLayout(rows=1, padding=self.title_padding, spacing=self.title_spacing)
        self.theTitleBar.bind(minimum_height=self.theTitleBar.setter('height'))

        self.theTitleBarS = ScrollView(do_scroll_y=False, pos_hint={'center_x': .5, 'center_y': .5},
                                       size_hint_y=kwargs['head_sizeY'])
        self.theTitleBarS.add_widget(self.theTitleBar)

        self.add_widget(self.theTitleBarS)

        self.theCarousel = Carousel(anim_move_duration=.075, scroll_timeout=0.0, scroll_distance=10000000)
        self.add_widget(self.theCarousel)

        def try_show_hint(title):
            return

            title = title.title()
            print('Attempting to show hint for: "%s"' % title)

            try:
                readHints = membank.get('readHints')['readHints']
            except:
                readHints = []

            if title in hints_title_text:
                hint_text = hints_title_text[title]
                hint_title = title

                if hint_text not in readHints:
                    next_bool_ = True
                    if title == 'Add Memory':
                        next_bool_ = False

                    OnboarderHint(title=hint_title, text=hint_text, next_bool=next_bool_).open()
                    readHints.append(hint_text)

            membank.put('readHints', readHints=readHints)

        global show_tab_by_title

        def show_tab_by_title(thisTH, title):
            for tabHead in thisTH.theTitleBar.children:
                if tabHead.text == title:
                    # showThisTab(tabHead)
                    tabHead.on_press()
                    tabHead.on_release()
                    break

        def showThisTab(self):
            for tabHead in thisTH.theTitleBar.children:
                tabHead.background_color = kwargs['inactive_color']
                if 'inactive_fg' in kwargs:
                    tabHead.color = kwargs['inactive_fg']
                else:
                    tabHead.color = (1, 1, 1, 1)

            self.background_color = kwargs['active_color']
            self.color = thisTH.active_fg  # (1,1,1,1)

            listOfSibblings = thisTH.theTitleBar.children
            listOfSibblings.reverse()

            newTab = listOfSibblings.index(self)
            thisTH.theCarousel.load_slide(thisTH.theCarousel.slides[newTab])

            listOfSibblings.reverse()

            try_show_hint(self.text)

        global addChildTab_TH

        def addChildTab_TH(a):
            print(a)
            this_fg_color = (1, 1, 1, 1)
            if 'inactive_fg' in kwargs:
                this_fg_color = kwargs['inactive_fg']

            thisTabHead = Button(text=a['name'], font_name=light, font_size=kwargs['head_fontSize'],
                                 background_normal=kwargs['head_background'], background_down='white',
                                 background_color=kwargs['inactive_color'], color=this_fg_color, halign='center',
                                 on_press=showThisTab, shorten=True, shorten_from='center',
                                 text_size=(.9 * kwargs['head_sizeX'] * Window.width, None))

            if a['name'] == 'LIVE':
                global conversationsTabText
                conversationsTabText = thisTabHead

            if a['name'] == 'STREAM':
                global feedsTabText
                feedsTabText = thisTabHead

            if a['on_load']:
                thisTabHead.on_release = a['on_load']

            self.theTitleBar.add_widget(thisTabHead, )
            try:
                a['wid'].parent.clear_widgets()
            except:
                pass
            self.theCarousel.add_widget(a['wid'])

            self.theTitleBar.size_hint_x = len(self.theTitleBar.children) * kwargs['head_sizeX']
            self.theTitleBarS.scroll_x += 0.00001

        for a in kwargs['slides']:
            addChildTab_TH(a)

        try:
            self.theTitleBar.children[-1].background_color = kwargs['active_color']
            self.theTitleBar.children[-1].font_name = 'Roboto-Regular.ttf'
            self.theTitleBar.children[-1].color = self.active_fg
            try_show_hint(self.theTitleBar.children[-1].text)
        except:
            pass

        self.theTitleBar.size_hint_x = len(self.theTitleBar.children) * kwargs['head_sizeX']


class StoreStrip(FloatLayout):
    def __init__(self, **kwargs):
        super(StoreStrip, self).__init__(**kwargs)

        dict__ = kwargs['dict__']

        def show_store(self):
            global StoreScreen_
            try:
                print(StoreScreen_)
            except:
                StoreScreen_ = StoreScreen()
                screenmanager.add_widget(StoreScreen_)

            screenmanager.current = 'Store Screen'
            StoreScreen_.load(dict__['store'])

        self.back_button = Button(
            background_down='white', background_normal='white',
            font_size=big_font, pos_hint={'center_x': .5, 'center_y': .5},
            on_release=show_store, markup=True)
        self.add_widget(self.back_button)

        self.icon = AsyncImage(allow_stretch=True, keep_ratio=True,
                               size_hint=(.2, .65),
                               pos_hint={'center_x': .5, 'center_y': .5})
        self.add_widget(self.icon)

        # self.front_box adds (logo, [name -- in personalized font and color, ig description, ig likes])


class StoreScreen(Screen, BoxLayout):
    def __init__(self, **kwargs):
        super(StoreScreen, self).__init__(**kwargs)

        self.name = 'Store Screen'

        # header
        # slide show of [best selling, *other pinned items...]

    def load(self, store_name):
        pass


class StoreCreationScreen(Screen, BoxLayout):
    def __init__(self, **kwargs):
        super(StoreCreationScreen, self).__init__(**kwargs)

        self.name = 'Store Creation Screen'

        store_name = TextInput(hint_text='Store name')
        email_address = TextInput(hint_text='Email address')
        city = TextInput(hint_text='City')
        state = TextInput(hint_text='State')

        instagram_label = Label(text='Import all your products from Instagram')
        instagram_handle = TextInput(hint_text='Instagram username')

        font_selection = Spinner()
        color_selection = Button()

        basic_info = BoxLayout(orientation='vertical')
        basic_info.add_widget(store_name)
        basic_info.add_widget(email_address)
        basic_info.add_widget(city)
        basic_info.add_widget(state)

        stock_loading_page = BoxLayout(orientation='vertical')
        stock_loading_page.add_widget(instagram_label)
        stock_loading_page.add_widget(instagram_handle)

        personalization_page = BoxLayout(orientation='vertical')
        personalization_page.add_widget(font_selection)
        personalization_page.add_widget(color_selection)

        store_creation_carousel = Carousel()
        store_creation_carousel.add_widget(basic_info)
        store_creation_carousel.add_widget(stock_loading_page)
        store_creation_carousel.add_widget(personalization_page)

        self.add_widget(store_creation_carousel)

        self.add_widget(proceed_button)


class TitleBar(FloatLayout):
    def __init__(self, **kwargs):
        super(TitleBar, self).__init__(**kwargs)

        color_ = kwargs['color']
        title = kwargs['title']

        self.size_hint_y = .08

        def show_prev_screen(icon):
            fprint(screen_history)

            global screen_history
            screen_history = screen_history[:-1]
            screenman.current = screen_history[-1]

            screen_history = screen_history[:-1]

        previous_icon = AsyncImage(source='atlas://assets/trail/previous_normal', allow_stretch=True, keep_ratio=True,
                                   size_hint=(.1, .3), pos_hint={'center_x': .05, 'center_y': .5},
                                   action=show_prev_screen)

        self.add_widget(Button(text=title, font_name=light, font_size=big_font, background_normal='white',
                               background_down='white', background_color=color_, text_size=(.7*Window.width, None),
                               pos_hint={'center_x': .5, 'center_y': .5}))

        if 'hamburger' in kwargs:
            self.add_widget(kwargs['hamburger'])

        if 'previous' in kwargs:
            self.add_widget(previous_icon)


class AddToCartButton(FloatLayout):
    def __init__(self, **kwargs):
        super(AddToCartButton, self).__init__(**kwargs)

        self.action = kwargs['action']

        dict_ = kwargs['dict']
        self.item_id = dict_['id']

        self.active = False

        self.active_button_color = (1,1,1,1)
        self.active_icon_color = dark_pink

        self.inactive_button_color = dark_pink
        self.inactive_icon_color = (1,1,1,1)

        self.button = Button(background_normal='', pos_hint={'center_x':.5, 'center_y':.5})
        self.button.on_release = self.click
        self.add_widget(self.button)

        self.icon = AsyncImage(source='assets/cart_icon.png', pos_hint={'center_x':.5, 'center_y':.5},
                               size_hint=(.5, .5), allow_stretch=True, keep_ratio=True)
        self.add_widget(self.icon)

        if self.active:
            self.button.background_color = self.active_button_color
            self.icon.color = self.active_icon_color

        else:
            self.button.background_color = self.inactive_button_color
            self.icon.color = self.inactive_icon_color

    def click(self):
        self.action()

        if self.active:
            self.active = False
            self.button.background_color = self.inactive_button_color
            self.icon.color = self.inactive_icon_color

        else:
            self.active = True
            self.button.background_color = self.active_button_color
            self.icon.color = self.active_icon_color


class ItemCard(StackLayout):
    def __init__(self, **kwargs):
        super(ItemCard, self).__init__(**kwargs)

        self.size_hint_y = .8

        dict_ = kwargs['dict']
        item_id = dict_['id']
        img_path = dict_['img_path']
        item_price = dict_['price']
        item_description = dict_['description']
        item_shipping_days = dict_['shipping_days']
        item_store = dict_['store']

        item_price = format_price(item_price)

        self.image = AsyncImage(source=img_path, allow_stretch=True, keep_ratio=False, size_hint=(1, .85))
        self.add_widget(self.image)

        self.item_desc_button = Button(text='[color=303030][font=%s][size=%s]%s[/size][/font][/color]\n'
                                            '[color=%s]N%s[/color][size=%s][color=a2a2a2][font=%s]\n%s[/font]'
                                            '[/color][/size]' % (
            bold, mid_font, item_description, green, item_price, mid_font, bold, item_store),
                                       background_normal='white', background_down='white', size_hint=(.8, .15), markup=True,
                                       text_size=(.72*Window.width, None), font_name=bold, font_size=mid_font)
        self.add_widget(self.item_desc_button)

        def do_add_to_cart():
            global cart

            if dict_['id'] in cart:
                cart.remove(dict_['id'])
            else:
                cart.append(dict_['id'])

            cart_bar_icon.update()

        self.add_to_cart_button = AddToCartButton(action=do_add_to_cart, dict=dict_, size_hint=(.2, .15))
        self.add_widget(self.add_to_cart_button)


class ItemStrip(Widget):
    def __init__(self, **kwargs):
        super(ItemStrip, self).__init__(**kwargs)


class SlideCard(FloatLayout):
    def __init__(self, **kwargs):
        super(SlideCard, self).__init__(**kwargs)

        item_ = kwargs['item']
        source_ = item_['img_path']
        description_ = item_['description']
        price_ = item_['price']
        store_ = item_['store']
        self.anim_time = kwargs['anim_time']
        dict_ = item_

        self.image = AsyncImage(color=(.45, .45, .45, 1), source=source_,
                                allow_stretch=True, keep_ratio=False,
                                pos_hint={'center_x':.5, 'center_y':.5})

        def do_add_to_cart():
            global cart

            if dict_['id'] in cart:
                cart.remove(dict_['id'])
            else:
                cart.append(dict_['id'])

            cart_bar_icon.update()

        self.add_to_cart_button = AddToCartButton(action=do_add_to_cart, dict=dict_, size_hint=(.2, 1))

        self.add_widget(self.image)

        self.box = BoxLayout(padding=7, pos_hint={'center_x':.5, 'center_y':.2}, size_hint_y=.2)
        self.box.add_widget(Label(text='%s[size=%s][color=%s]\nN%s[/color][color=cdcdcd]\n%s[/color]' % (description_, mid_font, green, price_, store_), font_name=bold, font_size=big_font,
                                  text_size=(.8*Window.width, None), markup=True))
        self.box.add_widget(self.add_to_cart_button)
        self.add_widget(self.box)

    def start_zoom(self):
        self.anim = Animation(duration=self.anim_time, size_hint=(1.1, 1.1))
        self.anim.start(self.image)

    def stop_zoom(self):
        if not hasattr(self, 'anim'):
            return

        self.anim.stop(self.image)
        self.image.size_hint = (1, 1)


class SlideShow(BoxLayout):
    def __init__(self, **kwargs):
        super(SlideShow, self).__init__(**kwargs)

        self.orientation = 'vertical'
        anim_time = 6

        self.size_hint_y = .7

        self.label = Label(text='Our Picks', color=(.2,.2,.2,1), font_name=bold, font_size=mid_font, size_hint_y=.1, text_size=(.9*Window.width, None))
        self.carousel = Carousel(anim_type='out_expo', anim_move_duration=2, loop=True)

        # self.add_widget(self.label)
        self.add_widget(self.carousel)

        items = kwargs['items']

        for item_ in items:
            self.carousel.add_widget(SlideCard(item=item_, anim_time=anim_time))

        def slide_roll(dt):
            next_index = self.carousel.slides.index(self.carousel.current_slide) + 1

            if next_index == len(self.carousel.slides):
                next_index = 0

            self.carousel.current_slide.stop_zoom()
            self.carousel.load_next()
            self.carousel.slides[next_index].start_zoom()

        Clock.schedule_interval(slide_roll, anim_time)


class Aisle(ScrollView):
    def __init__(self, **kwargs):
        super(Aisle, self).__init__(**kwargs)

        aisle = kwargs['aisle']

        self.do_scroll_x = False

        self.grid = GridLayout(cols=1, spacing=25)
        self.grid.bind(minimum_height=self.grid.setter('height'))
        self.add_widget(self.grid)

        if 'autoload' in kwargs:
            self.load()

    def load(self):
        total_y = 0

        self.grid.add_widget(
            Label(text='\nOur Picks', color=(.2, .2, .2, 1), font_name=bold, font_size=mid_font, size_hint_y=.1,
                  text_size=(.9 * Window.width, None)))
        total_y += .1

        self.slide = SlideShow(items=fake_items_2)
        total_y += self.slide.size_hint_y
        self.grid.add_widget(self.slide)

        self.grid.add_widget(
            Label(text='Explore', color=(.2, .2, .2, 1), font_name=bold, font_size=mid_font, size_hint_y=.1,
                  text_size=(.9 * Window.width, None)))
        total_y += .1

        for item in fake_items:
            itemcard = ItemCard(dict=item)
            total_y += itemcard.size_hint_y
            self.grid.add_widget(itemcard)

        self.grid.size_hint_y = total_y

        self.size_hint_y += .00001


class HomeTab(BoxLayout):
    def __init__(self, **kwargs):
        super(HomeTab, self).__init__(**kwargs)

        random_carousel = Aisle(aisle='random', autoload=True)

        mall_aisles = [{'wid': random_carousel, 'name': 'Random', 'on_load': None}]
        for aisle in ('Clothing', 'Gadgets', 'Digital', 'Crafts'):
            this_aisle = Aisle(aisle=aisle.lower())
            mall_aisles.append({'name': aisle, 'wid': this_aisle, 'on_load': this_aisle.load()})

        the_tabs_holder = TabsHolder(head_sizeX=.3, head_sizeY=0.08, slides=mall_aisles, head_fontSize=mid_font,
                                     head_background='white', inactive_color=(.45, .45, .45, .55),
                                     font_color=(1, 1, 1, 1),
                                     active_color=(1, 1, 1, 1))
        self.add_widget(the_tabs_holder)


class TrendsTab(ScrollView):
    def __init__(self, **kwargs):
        super(TrendsTab, self).__init__(**kwargs)

        self.grid = GridLayout(cols=1)
        self.grid.bind(minimum_height=self.grid.setter('height'))
        self.add_widget(self.grid)

        try:
            ext_app_dict = membank.get('ext_app')['value']
        except:
            ext_app_dict = None

        def prompt_external_app_login(self):
            external_app = self.text.lower()

            Webview(
                address='http://{web_address}/{external_app}_login'.format(
                    web_address=webscript.servers_ip,
                    external_app=external_app,)
            )

        if not ext_app_dict:
            trends_front_page = FloatLayout()

            trends_front_page.add_widget(
                Button(text='Find out what your friends are craving!',
                       pos_hint={'center_x': .5, 'center_y': .5},
                       font_name=bold, font_size=big_font, color=(0,0,0,.8),
                       background_down='white', background_normal='white',
                       text_size=(None, .5*Window.height),
                       valign='top'))

            trends_front_page.add_widget(
                Button(text='Facebook', font_name=bold, font_size=mid_font,
                       background_normal='white',
                       pos_hint={'center_x': .5, 'center_y': .475},
                       size_hint=(.75, .09),
                       on_release=prompt_external_app_login,
                       background_color=get_color_from_hex(fb_color)))

            trends_front_page.add_widget(
                Button(text='Twitter', font_name=bold, font_size=mid_font,
                       background_normal='white',
                       pos_hint={'center_x': .5, 'center_y': .35},
                       size_hint=(.75, .09),
                       on_release=prompt_external_app_login,
                       background_color=get_color_from_hex(tw_color)))

            self.grid.add_widget(trends_front_page)

        else:
            trends = webscript.get_trends(membank.get('ext_app')['value'])



class StoresTab(ScrollView):
    def __init__(self, **kwargs):
        super(StoresTab, self).__init__(**kwargs)

        self.grid = GridLayout(cols=1)
        self.grid.bind(minimum_height=self.grid.setter('height'))
        self.add_widget(self.grid)

        try:
            stores = membank.get('stores')['stores']
        except:
            stores = []

        for store_id in stores:
            self.grid.add_widget(StoreStrip(store_id=store_id))

        if len(self.grid.children) == 0:
            self.grid.add_widget(Button(text='Stores'))


class ProfileTab(ScrollView):
    def __init__(self, **kwargs):
        super(ProfileTab, self).__init__(**kwargs)

        self.grid = GridLayout(cols=1)
        self.grid.bind(minimum_height=self.grid.setter('height'))
        self.add_widget(self.grid)

        self.grid.add_widget(Button(text='Profile'))


class CheckoutScreen(Screen):
    def __init__(self, **kwargs):
        super(CheckoutScreen, self).__init__(**kwargs)

        self.name = 'Checkout Screen'

        self.box = BoxLayout(orientation='vertical')
        self.add_widget(self.box)

        self.box.add_widget(TitleBar(title='Checkout', previous=True, color=dark_pink))

        self.cart_carousel = GridLayout(rows=1)
        self.cart_carousel.bind(minimum_height=self.cart_carousel.setter('height'))
        self.cart_carousel_scroll = ScrollView(do_scroll_x=False, size_hint_y=.4)
        self.cart_carousel_scroll.add_widget(self.cart_carousel)
        self.box.add_widget(self.cart_carousel_scroll)

        self.suggestions_demarcation_label = Label(text='Subtotal', font_name=bold, color=(.2, .2, .2, .8),
                                                   size_hint_y=.025, text_size=(.94 * Window.width, None),
                                                   font_size=mid_font)
        self.box.add_widget(self.suggestions_demarcation_label)

        self.sub_total_label = Label(font_name=light, font_size=biggest_font, text_size=(.93*Window.width, None),
                                     halign='right', size_hint_y=.15, color=get_color_from_hex(green))
        self.box.add_widget(self.sub_total_label)

        self.suggestions_demarcation_label = Label(text='You should also get', font_name=bold, color=(.2, .2, .2, .8),
                                                   size_hint_y=.025, text_size=(.94*Window.width, None),
                                                   font_size=mid_font)
        self.box.add_widget(self.suggestions_demarcation_label)

        self.suggestions_scroll = ScrollView(do_scroll_y=False, size_hint_y=.4)
        self.box.add_widget(self.suggestions_scroll)

        self.suggestions_grid = GridLayout(rows=1)
        self.suggestions_grid.bind(minimum_height=self.suggestions_grid.setter('height'))

    def refresh(self):
        total_cost = 0

        for item_ in cart:
            full_dict = membank.get('mall_cache')['mall_cache'][item_]
            total_cost += float(full_dict['price'])

            self.cart_carousel.add_widget(SlideCard(item=full_dict, anim_time=0))

        self.cart_carousel.size_hint_x = len(self.cart_carousel.children) * .85
        self.cart_carousel_scroll.size_hint_x += .000001

        self.sub_total_label.text = str(total_cost)
        suggestions = []#webscript.get_suggestions(cart)

        for suggestion in suggestions:
            itemcard = ItemCard(dict=suggestion)
            self.suggestions_grid.add_widget(itemcard)

        self.suggestions_grid.size_hint_x = len(self.suggestions_grid.children) * .35

    def on_enter(self):
        self.refresh()
        Window.clearcolor = (1, 1, 1, 1)


class ProductScreen(Screen):
    def __init__(self, **kwargs):
        super(ProductScreen, self).__init__(**kwargs)

        self.name = 'Product Screen'


class StoreScreen(Screen):
    def __init__(self, **kwargs):
        super(StoreScreen, self).__init__(**kwargs)

        self.name = 'Store Screen'


class MallScreen(Screen):
    def __init__(self, **kwargs):
        super(MallScreen, self).__init__(**kwargs)

        def sync_cart():
            global cart
            cart = []

        threading.Timer(0, sync_cart).start()

        class TitleBar(FloatLayout):
            def __init__(self, **kwargs):
                super(TitleBar, self).__init__(**kwargs)

                self.size_hint_y = .09
                self.add_widget(Button(text=__title__, background_normal='white', background_down='white',
                                       font_name=emblem_font, font_size=bigger_font,
                                       pos_hint={'center_x':.5, 'center_y':.5}, color=emblem_color))

                self.stack = BoxLayout(pos_hint={'center_x':.5, 'center_y':.5})
                self.add_widget(self.stack)

                class TitleBarIcon(FloatLayout):
                    def __init__(self, **kwargs):
                        super(TitleBarIcon, self).__init__(**kwargs)

                        def show_checkout_screen(self):
                            global CheckoutScreen_
                            try:
                                print(CheckoutScreen_)
                            except:
                                CheckoutScreen_ = CheckoutScreen()
                                screenmanager.add_widget(CheckoutScreen_)

                            screenmanager.current = 'Checkout Screen'

                        icon = kwargs['icon']
                        self.icon = AsyncImage(source=icon, allow_stretch=True, keep_ratio=True,
                                               pos_hint={'center_x':.5, 'center_y':.5}, size_hint=(.8, .8))
                        self.counter = Button(background_normal='white', background_down='white',
                                              size=(.075*Window.width, .075*Window.width),
                                              size_hint=(None, None), pos_hint={'center_x':.5, 'center_y':.5},
                                              font_name=bold, font_size=mid_font, on_release=show_checkout_screen)

                        # self.add_widget(self.icon)
                        self.add_widget(self.counter)

                        self.update()

                    def update(self):
                        self.counter.text = str(len(cart))

                        if self.counter.text == '0':
                            self.counter.color = self.counter.background_color = (0, 0, 0, 0)
                        else:
                            self.counter.color = (1, 1, 1, 1)
                            self.counter.background_color = blue

                global cart_bar_icon
                cart_bar_icon = TitleBarIcon(icon='assets/cart_icon.png', size_hint_x=.15)
                self.stack.add_widget(cart_bar_icon)
                self.stack.add_widget(Widget())
                self.stack.children.reverse()

        class HomeCarousel(Carousel):
            def __init__(self, **kwargs):
                super(HomeCarousel, self).__init__(**kwargs)

                self.min_move = 20000
                self.anim_move_duration = .15
                self.scroll_timeout = 0

                self.add_widget(HomeTab())
                self.add_widget(TrendsTab())
                self.add_widget(StoresTab())
                self.add_widget(ProfileTab())

        class NavBar(BoxLayout):
            def __init__(self, **kwargs):
                super(NavBar, self).__init__(**kwargs)

                navbar = self
                self.active_background_color = kwargs['active_background_color']
                self.inactive_background_color = kwargs['inactive_background_color']

                class NavBarButton(FloatLayout):
                    def __init__(self, **kwargs):
                        super(NavBarButton, self).__init__(**kwargs)

                        self.back_button = Button(background_normal='white', background_color=navbar.inactive_background_color, pos_hint={
                            'center_x':.5, 'center_y':.5
                        })
                        self.back_button.on_release = self.select
                        self.add_widget(self.back_button)

                        self.icon = AsyncImage(source=kwargs['icon'], allow_stretch=True, keep_ratio=True, pos_hint={
                            'center_x':.5, 'center_y':.5
                        }, size_hint=(.45, .45))
                        self.add_widget(self.icon)

                    def select(self):
                        for navbarbutton in navbar.children:
                            navbarbutton.back_button.background_color = navbar.inactive_background_color

                        self.back_button.background_color = navbar.active_background_color

                        do_vibrate(.17)

                        chosen_index = 3 - navbar.children.index(self)
                        logging.info(str(chosen_index))
                        chosen_slide = homecarousel.slides[chosen_index]
                        homecarousel.load_slide(chosen_slide)

                for icon_ in ['home', 'trends', 'stores', 'profile']:
                    fullpath = 'assets/%s_icon.png' % icon_
                    self.add_widget(NavBarButton(icon=fullpath))

                self.children[-1].select()

        self.name = 'Mall Screen'

        self.box = BoxLayout(orientation='vertical')
        self.add_widget(self.box)

        self.box.add_widget(TitleBar())

        homecarousel = HomeCarousel()
        self.box.add_widget(homecarousel)

        self.box.add_widget(NavBar(active_background_color=pink, inactive_background_color=dark_pink, size_hint_y=.1))


screenmanager = ScreenManager()
screenmanager.add_widget(MallScreen())


class BerriesApp(App):
    def build(self):
        return screenmanager

    def _key_handler(self, *args):

        app__ = self
        key = args[1]

        if key in (1000, 27):
            try:
                import android
                android.hide_keyboard()
            except ImportError:
                pass

            global screen_history
            screen_history = screen_history[:-1]

            try:
                screenmanager.current = screen_history[-1]
            except:
                return

            print('New screen_history: %s' % screen_history)

            # screen_history = screen_history[:-1]

            screenmanager.transition = SwapTransition(
                direction='right', duration=.1)
            # float_for_sm.current = screen_history[-1]
            screenmanager.transition = SlideTransition(duration=.1)

            # move_paper_sound.play()

        return True


BerriesApp().run()
