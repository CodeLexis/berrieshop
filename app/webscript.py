import requests
import sys

servers_ip = 'http://localhost:8080'

if sys.platform != 'win32':
    servers_ip = 'http://some-weird-address'


def get_suggestions(cart):
    return requests.get(servers_ip, {'cart': cart}).json()


def get_trends(ext_app_dict):
    pass


def get_stores():
    pass


def get_store_head():
    pass